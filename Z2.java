package Z2;
import java.util.Scanner;
public class Z2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Заработная плата в час (в долларах)(не меньше 8$): ");
        double zp = in.nextDouble();
        if(zp < 8) {
            System.out.println("Работник не может получать меньше 8 долларов в час ");
            return;
        }
        System.out.println("Число проработанных часов в неделе(не более 60): ");
        byte hours = in.nextByte();
        if(hours>60) {
            System.out.println("Работник не может работать более 60 часов в неделю ");
            return;
        }
        double zarpl = 0;
        for (byte i=1;i<=hours;i++) {
            if (i<=40) {
                zarpl+=zp;
            } else {
                zarpl+=1.5*zp;
            }
        }
        System.out.print("Заработная плата работника в неделю составляет: ");
        System.out.printf("%.2f", zarpl);
        System.out.print(" долларов");
    }
}

