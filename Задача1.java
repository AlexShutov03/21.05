package Задача1;
import java.util.Scanner;
public class Задача1 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Введите 1-ое число: ");
        double a = in.nextDouble();
        System.out.println("Введите 2-ое число: ");
        double b = in.nextDouble();
        System.out.println("Введите знак операции: ");
        char oper = in.next().charAt(0);
        System.out.println("Результат операции: ");
        switch (oper) {
            case '+': System.out.print(a+b);
                break;
            case '-': System.out.print(a-b);
                break;
            case '*': System.out.print(a*b);
                break;
            case '/': System.out.print(a/b);
                break;
            case '%': System.out.print(a%b);
                break;
            default:  System.out.print("Неверный ввод данных");
                break;
        }
    }
}